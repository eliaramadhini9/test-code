const express = require('express')
const router = express.Router()
const AppController = require('../controllers/appController');

router.get('/appSatu', AppController.appSatu)
router.get('/appDua', AppController.appDua)
router.get('/appTiga', AppController.appTiga)

module.exports = router;
