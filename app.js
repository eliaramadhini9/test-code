const express = require('express');
const app = express();
const appRoutes = require('./routes/appRoutes');


app.use(express.static('public'));

app.use('/', appRoutes)

app.listen(8000, () => {
  console.log('Running on port 8080!');
})
