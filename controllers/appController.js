const fetch = require('node-fetch');
const salary_data = require('../json/salary_data').array;
const sensor_data = require('../json/sensor_data').array;

class AppController {
  async appSatu(req, res) {
    try {
      let user = await fetch(`http://jsonplaceholder.typicode.com/users`)
      let getData = await user.json()

      let getCurrency = await fetch(`https://free.currconv.com/api/v7/convert?q=USD_IDR&compact=ultra&apiKey=604c09c967706aff31a7`)
      let converter = await getCurrency.json()

      var newData = []
      for (var i = 0; i < getData.length; i++) {
        for (var j = 0; j < salary_data.length; j++) {
          if (getData[i].id === salary_data[j].id) {
            newData[i] = {
              id: getData[i].id,
              name: getData[i].name,
              username: getData[i].username,
              email: getData[i].email,
              address: getData[i].address,
              phone: getData[i].phone,
              salary: {
                salaryInIDR: salary_data[j].salaryInIDR,
                salaryInUSD: (salary_data[j].salaryInIDR) / converter.USD_IDR,
              }
            }
          }
        }
      }

      return res.json({
        status: 'Success',
        data: newData
      })
    } catch (e) {
      return res.json({
        status: 'Error',
        message: e
      })
    }
  }

  async appDua(req, res) {
    try {
      const {
        room,
        day
      } = req.query
      let newData = []
      for (let i = 0; i < sensor_data.length; i++) {
        let day = new Date(sensor_data[i].timestamp * 1000).toLocaleDateString('en-US', {
          weekday: 'long'
        })
        if (sensor_data[i].roomArea === room && day === day)
          newData.push(sensor_data[i])
      }

      let middle = Math.floor(newData.length / 2),
        median
      if (newData.length % 2) {
        median = (newData[middle - 1] + newData[middle]) / 2
      } else {
        median = middle
      }

      let result = {
        temperature: {
          min: Math.min.apply(Math, newData.map(a => a.temperature)),
          max: Math.max.apply(Math, newData.map(a => a.temperature)),
          median: newData[median].temperature,
          avg: newData.map(a => a.temperature).reduce((a, b) => a + b) / newData.length
        },
        humidity: {
          min: Math.min.apply(Math, newData.map(a => a.humidity)),
          max: Math.max.apply(Math, newData.map(a => a.humidity)),
          median: newData[median].humidity,
          avg: newData.map(a => a.humidity).reduce((a, b) => a + b) / newData.length
        }
      }
      return res.json({
        status: 'Success',
        data: result
      })
    } catch (e) {
      console.log(e)
      return res.json({
        status: 'Error',
        message: e
      })
    }
  }

  async appTiga(req, res) {
    try {
      let sensor = []
      let arr = ['Jakarta', 'Bogor', 'Cirebon', 'Semarang', 'Bandung']
      for (var i = 0; i < arr.length; i++) {
        let get = await fetch(`http://api.weatherapi.com/v1/current.json?key=6b5e5c0003db4b07b8572128213103&q=${arr[i]}&aqi=yes`)
        let result = await get.json()

        sensor[i] = {
          temperature: result.current.temp_c,
          humidity: result.current.humidity
        }
      }

      return res.json({
        status: 'Success',
        data: sensor
      })
    } catch (e) {
      console.log(e);
      return res.json({
        status: 'Error',
        message: e
      })
    }
  }
}

module.exports = new AppController
